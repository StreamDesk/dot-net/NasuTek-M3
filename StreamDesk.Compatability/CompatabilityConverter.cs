﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NasuTek.M3.Streams.Database;
using M3StreamsDatabase = NasuTek.M3.Streams.Database.StreamsDatabase;
using M3ChatEmbed = NasuTek.M3.Streams.Database.ChatEmbed;
using M3StreamEmbed = NasuTek.M3.Streams.Database.StreamEmbed;
using M3Provider = NasuTek.M3.Streams.Database.Provider;

namespace StreamDesk.Compatability
{
    public static class CompatabilityConverter
    {
        public static void ConvertEmbeds(this StreamDeskDatabase currentDb, M3StreamsDatabase newDb)
        {
            foreach (var chatEmbedNew in currentDb.ChatEmbeds.Select(chatEmbed => new M3ChatEmbed { EmbedFormat = chatEmbed.EmbedFormat, FriendlyName = chatEmbed.FriendlyName, IrcServer = chatEmbed.IrcServer, Name = chatEmbed.Name }))
            {
                newDb.ChatEmbeds.Add(chatEmbedNew);
            }

            foreach (var streamEmbedNew in currentDb.StreamEmbeds.Select(streamEmbed => new M3StreamEmbed { EmbedFormat = streamEmbed.EmbedFormat, FriendlyName = streamEmbed.FriendlyName, Name = streamEmbed.Name }))
            {
                newDb.StreamEmbeds.Add(streamEmbedNew);
            }
        }
        public static void ConvertEmbeds(this M3StreamsDatabase currentDb, StreamDeskDatabase newDb)
        {
            foreach (var chatEmbedNew in currentDb.ChatEmbeds.Select(chatEmbed => new ChatEmbed { EmbedFormat = chatEmbed.EmbedFormat, FriendlyName = chatEmbed.FriendlyName, IrcServer = chatEmbed.IrcServer, Name = chatEmbed.Name }))
            {
                newDb.ChatEmbeds.Add(chatEmbedNew);
            }

            foreach (var streamEmbedNew in currentDb.StreamEmbeds.Select(streamEmbed => new StreamEmbed { EmbedFormat = streamEmbed.EmbedFormat, FriendlyName = streamEmbed.FriendlyName, Name = streamEmbed.Name }))
            {
                newDb.StreamEmbeds.Add(streamEmbedNew);
            }
        }

        public static void ConvertProviders(this Provider provider, M3Provider newProvider)
        {
            newProvider.Name = provider.Name;
            newProvider.Description = provider.Description;
            newProvider.Web = provider.Web;
            newProvider.Pinned = provider.Pinned;

            foreach (var media in provider.Medias)
            {
                var stream = new Stream { Name = media.Name, Description = media.Description, Web = media.Web, ChatEmbed = media.ChatEmbed, StreamGuid = media.StreamGuid, StreamEmbed = media.StreamEmbed, Tags = media.Tags, Size = media.Size };
                foreach (var newEmbedData in media.ChatEmbedData.Select(embedData => new StreamDeskProperty { Name = embedData.Name, Value = embedData.Value }))
                {
                    stream.ChatEmbedData.Add(newEmbedData);
                }
                foreach (var newEmbedData in media.StreamEmbedData.Select(embedData => new StreamDeskProperty { Name = embedData.Name, Value = embedData.Value }))
                {
                    stream.StreamEmbedData.Add(newEmbedData);
                }
                newProvider.Streams.Add(stream);
            }

            foreach (var subProvider in provider.SubProviders)
            {
                var newSubProvider = new M3Provider();
                subProvider.ConvertProviders(newSubProvider);
                newProvider.SubProviders.Add(newSubProvider);
            }
        }

        public static void ConvertProviders(this M3Provider provider, Provider newProvider)
        {
            newProvider.Name = provider.Name;
            newProvider.Description = provider.Description;
            newProvider.Web = provider.Web;
            newProvider.Pinned = provider.Pinned;

            foreach (var media in provider.Streams)
            {
                var stream = new Media { Name = media.Name, Description = media.Description, Web = media.Web, ChatEmbed = media.ChatEmbed, StreamGuid = media.StreamGuid, StreamEmbed = media.StreamEmbed, Tags = media.Tags, Size = media.Size };
                foreach (var newEmbedData in media.ChatEmbedData.Select(embedData => new EmbedData { Name = embedData.Name, Value = embedData.Value }))
                {
                    stream.ChatEmbedData.Add(newEmbedData);
                }
                foreach (var newEmbedData in media.StreamEmbedData.Select(embedData => new EmbedData { Name = embedData.Name, Value = embedData.Value }))
                {
                    stream.StreamEmbedData.Add(newEmbedData);
                }
                newProvider.Medias.Add(stream);
            }

            foreach (var subProvider in provider.SubProviders)
            {
                var newSubProvider = new Provider();
                subProvider.ConvertProviders(newSubProvider);
                newProvider.SubProviders.Add(newSubProvider);
            }
        }

    }
}
