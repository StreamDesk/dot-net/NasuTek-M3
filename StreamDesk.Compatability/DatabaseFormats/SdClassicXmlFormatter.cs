﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using NasuTek.M3.Streams;
using M3StreamsDatabase = NasuTek.M3.Streams.Database.StreamsDatabase;

namespace StreamDesk.Compatability.DatabaseFormats {
    public class SdClassicXmlFormatter : IDatabaseFormatter {
        public string FormatName {
            get { return "StreamDesk Database 1.3 (XML)"; }
        }

        public string FileExtension {
            get { return ".sdx"; }
        }

        public M3StreamsDatabase Read(System.IO.Stream file) {
            var formatter = new XmlSerializer(typeof (StreamDeskDatabase));
            var currentDb = (StreamDeskDatabase) formatter.Deserialize(file);
            var newDb = new M3StreamsDatabase {Name = "Converted SD 1.3 Database"};

            currentDb.ConvertEmbeds(newDb);
            currentDb.Root.ConvertProviders(newDb.Root);

            return newDb;
        }

        public void Write(System.IO.FileStream file, M3StreamsDatabase StreamsDatabase) {
            var formatter = new XmlSerializer(typeof (StreamDeskDatabase));
            var newDb = new StreamDeskDatabase();

            StreamsDatabase.ConvertEmbeds(newDb);
            StreamsDatabase.Root.ConvertProviders(newDb.Root);

            formatter.Serialize(file, newDb);
        }
    }
}
