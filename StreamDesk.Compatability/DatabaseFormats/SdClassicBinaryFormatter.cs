﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using NasuTek.M3.Streams;
using M3StreamsDatabase = NasuTek.M3.Streams.Database.StreamsDatabase;

namespace StreamDesk.Compatability.DatabaseFormats {
    public class SdClassicBinaryFormatter : IDatabaseFormatter {
        private class SdCompatBinder : SerializationBinder {
            public override void BindToName(Type serializedType, out string assemblyName, out string typeName)
            {
                if (serializedType.Assembly.FullName == "mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")
                {
                    assemblyName = serializedType.Assembly.FullName;
                    typeName = serializedType.FullName.Replace("StreamDesk.Compatability", "StreamDesk.Core").Replace(SDCompat_Version.FullVersion, "2.2.1.0");
                    return;
                }

                if (serializedType.FullName.Contains("StreamDesk.Compatability")) {
                    assemblyName = "StreamDesk.Core, Version=2.2.1.0, Culture=neutral, PublicKeyToken=null";
                    typeName = serializedType.FullName.Replace("StreamDesk.Compatability", "StreamDesk.Core").Replace(SDCompat_Version.FullVersion, "2.2.1.0");
                    return;
                }

                base.BindToName(serializedType,out assemblyName,out typeName);
            }

            public override Type BindToType(string assemblyName, string typeName) {
                switch (assemblyName) {
                    case "StreamDesk.Core, Version=2.2.0.0, Culture=neutral, PublicKeyToken=null":
                    case "StreamDesk.Core, Version=2.2.1.0, Culture=neutral, PublicKeyToken=null":
                        return typeof (SdClassicBinaryFormatter).Assembly.GetType(typeName.Replace("StreamDesk.Core", "StreamDesk.Compatability"));
                    case "mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089":
                        return Type.GetType(typeName.Replace("StreamDesk.Core", "StreamDesk.Compatability"));
                }

                return null;
            }
        }

        public string FormatName {
            get { return "StreamDesk Database 1.3 (Binary)"; }
        }

        public string FileExtension {
            get { return ".sdb"; }
        }

        public M3StreamsDatabase Read(System.IO.Stream file) {
            var formatter = new BinaryFormatter {Binder = new SdCompatBinder()};
            var currentDb = (StreamDeskDatabase) formatter.Deserialize(file);
            var newDb = new M3StreamsDatabase {Name = "Converted SD 1.3 Database"};
            
            currentDb.ConvertEmbeds(newDb);
            currentDb.Root.ConvertProviders(newDb.Root);

            return newDb;
        }

        public void Write(System.IO.FileStream file, M3StreamsDatabase StreamsDatabase) {
            var formatter = new BinaryFormatter { Binder = new SdCompatBinder() };
            var newDb = new StreamDeskDatabase();

            StreamsDatabase.ConvertEmbeds(newDb);
            StreamsDatabase.Root.ConvertProviders(newDb.Root);

            formatter.Serialize(file, newDb);
        }
    }
}
