﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NasuTek.M3.DatabaseFormats
{
    class SDJsonFormatter :IDatabaseFormatter
    {
        public string FormatName
        {
            get { return "StreamDesk Database 3.0 (Json)"; }
        }

        public string FileExtension
        {
            get { return ".json"; }
        }

        public Database.StreamDeskDatabase Read(System.IO.Stream file)
        {
            throw new NotImplementedException();
        }

        public void Write(System.IO.FileStream file, Database.StreamDeskDatabase streamDeskDatabase)
        {
            throw new NotImplementedException();
        }
    }
}
