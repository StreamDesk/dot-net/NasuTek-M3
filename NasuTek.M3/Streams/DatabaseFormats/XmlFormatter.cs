﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using NasuTek.M3.Streams.Database;

namespace NasuTek.M3.Streams.DatabaseFormats
{
    public class XmlFormatter : IDatabaseFormatter
    {
        public string FormatName
        {
            get { return "Streams Database 2.0 (XML)"; }
        }

        public string FileExtension
        {
            get { return ".sdnx"; }
        }

        public StreamsDatabase Read(System.IO.Stream file)
        {
            var formatter = new XmlSerializer(typeof(StreamsDatabase));
            return(StreamsDatabase)formatter.Deserialize(file);
        }

        public void Write(System.IO.FileStream file, StreamsDatabase streamsDatabase)
        {
            var formatter = new XmlSerializer(typeof(StreamsDatabase));
            formatter.Serialize(file, streamsDatabase);
        }
    }
}
